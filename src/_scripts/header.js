// import Media from './settings/media';
export default {
    menuSelector: '.js-menu',
    headerSelector: '.header',
    overlaySelector: '.js-overlay',
    exitSelector: '.js-hamburger-open',
    hamburgerSelector: '.js-hamburger',
    
  init() {
    let self= this;

    $(document).on("click", this.exitSelector, function(event) {
      $(self.menuSelector).removeClass('open');
      $(self.overlaySelector).fadeOut();
    //   $(self.headerSelector).removeClass('open');
      $(self.exitSelector).removeClass('hamburger--active').addClass('js-hamburger').removeClass('js-hamburger-open');
    });
    $(this.overlaySelector).click(() => {
      $(self.exitSelector).removeClass('hamburger--active');
      $(self.menuSelector).removeClass('open');
      $(self.overlaySelector).fadeOut();
      $(self.headerSelector).removeClass('open'); 
    });
   
    $(document).on("click", this.hamburgerSelector, function(event) {
        
      $(self.hamburgerSelector).addClass('hamburger--active').removeClass('js-hamburger').addClass('js-hamburger-open');
      $(self.menuSelector).addClass('open');
      $(self.overlaySelector).fadeIn();      
    //   $(self.headerSelector).addClass('open'); 
    });
  

    $('a.page-scroll').bind('click', function(event) {
      var self = $(this);
      if ($('a.page-scroll').hasClass('menu__item--active')) {
          $('a.page-scroll').removeClass('menu__item--active');
          self.addClass('menu__item--active');
      } else {
          self.addClass('menu__item--active');
      }
      $('html, body').stop().animate({
          scrollTop: $(self.attr('href')).offset().top-60  
      }, 500,
      // function (){
      //       $(".header").css({background:'white',
      //         transition:'.5s',
      //         'border-bottom': '1px solid #3dcf70'
              
      //       });

      //     }
        );   
      event.preventDefault();
  });
  $(window).scroll(function(){
      var window_top = $(window).scrollTop() + 12; // the "12" should equal the margin-top value for nav.stick
      var div_top = $('.header').offset().top;
          // if ($(window).width() > "768" && $(".header").offset().top > 720) {
          // $(".header").css({background:'white',
          //     transition:'.5s',
          //     'border-bottom': '1px solid #3dcf70'});
          // }else if ($(window).width() <= "768" ){
          //     $(".header").css({background:'#3dcf70',
          //     transition:'.5s'});

          // }else{
          //     $(".header").css({background:'rgba(255, 255, 255, 0.40)',
          //     transition:'.5s',
          //     'border-bottom': '1px solid transparent'});
          // }
      });
  var aChildren = $(".menu a"); 
  var aArray = []; 
  for (var i=0; i < aChildren.length; i++) {    
      var aChild = aChildren[i];
      var ahref = $(aChild).attr('href');
      aArray.push(ahref);
  } 

  $(window).scroll(function(){
      var windowPos = $(window).scrollTop(); 
      var windowHeight = $(window).height(); 
      var docHeight = $(document).height();

      for (var i=0; i < aArray.length; i++) {
          var theID = aArray[i];
          var divPos = $(theID).offset().top - 100; 
          var divHeight = $(theID).height(); 
          if (windowPos >= divPos && windowPos < (divPos + divHeight)) {
              $("a[href='" + theID + "']").addClass("menu__item--active");
          } else {
              $("a[href='" + theID + "']").removeClass("menu__item--active");
          }
      }
      if(windowPos + windowHeight == docHeight) {
          if (!$(".menu a:last-child ").hasClass("menu__item--active")) {
              var navActiveCurrent = $(".menu__item--active").attr("href");
              $("a[href='" + navActiveCurrent + "']").removeClass("menu__item--active");
              $(".menu a:last-child").addClass("menu__item--active");
          }
      }
    });
  }
}